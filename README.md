# disable the current service
`/etc/init.d/<name> stop`

# copy the systemd unit into place
`cp <name>.service /etc/systemd/system/`

# edit whatever you need to to make it match your setup  
* leave the `pid` and `runtime` stuff alone if you aren't sure, it's to drop a `pid` at `/run/<name>/<name>.pid`
* `WorkingDirectory` is where you have your git checkout

# try it out
`systemctl start <name>.service`

# if everything works, make it run at boot
`systemctl enable <name>.service`

# ... and disable and remove the old init service
`update-rc.d <name> disable`

`rm /etc/init.d/<name>` or `unlink /etc/init.d/<name>` if you are using symlinks

# rinse and repeat for the others
